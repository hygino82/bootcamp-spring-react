package com.devsuperior.dsmeta.controllers;

import com.devsuperior.dsmeta.dto.SaleDTO;
import com.devsuperior.dsmeta.services.SaleService;
import com.devsuperior.dsmeta.services.SmsService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/sale")
public class SaleController {
    private final SaleService service;
    private final SmsService smsService;

    public SaleController(SaleService service, SmsService smsService) {
        this.service = service;
        this.smsService = smsService;
    }

    @GetMapping("/all")
    public ResponseEntity<Page<SaleDTO>> findAll(Pageable pageable) {
        var pageDto = service.findAll(pageable);

        return ResponseEntity.ok().body(pageDto);
    }

    @GetMapping
    public ResponseEntity<Page<SaleDTO>> findSales(
            @RequestParam(value = "minDate", defaultValue = "") String minDate,
            @RequestParam(value = "maxDate", defaultValue = "") String maxDate,
            Pageable pageable) {
        Page<SaleDTO> pageDto = service.findSales(minDate, maxDate, pageable);

        return ResponseEntity.ok().body(pageDto);
    }


    @GetMapping("/{id}/notification")
    public void notitySms(@PathVariable Long id) {
        smsService.sendSms(id);
    }
}
