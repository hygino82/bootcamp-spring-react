export type Sale = {
    id: number;
    sellerName: string;
    deals: number;
    date: string;
    visited: number;
    amount: number;
}